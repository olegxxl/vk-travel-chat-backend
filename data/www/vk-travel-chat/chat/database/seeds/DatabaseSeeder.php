<?php

use App\Http\Controllers\ChatController;
use App\Http\Controllers\MessageController;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    const VK_OLEG = 217740281; #1
    const VK_ARTEM = 18633531; #2
    const VK_ALEX = 152234148; #3
    const VK_MISHA = 263652051;#4
    const VK_DEN = 1930617;    #5
    const VK_PASHKA = 1;       #6

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $f = Faker\Factory::create('ru_RU');
        DB::table('chats')->insert([
            'name' => '054Ч',
            'city' => 'Санкт-Петербург',
            'date' => $f->dateTimeBetween('now', '+30 days'),
            'type' => 'train',
            'created_at' => \Carbon\Carbon::now()->__toString(),
            'updated_at' => \Carbon\Carbon::now()->__toString(),
        ]);
        DB::table('chats')->insert([
            'name' => 'Интурист',
            'city' => 'Ростов-на-Дону',
            'date' => $f->dateTimeBetween('now', '+30 days'),
            'type' => 'hotel',
            'created_at' => \Carbon\Carbon::now()->__toString(),
            'updated_at' => \Carbon\Carbon::now()->__toString(),
        ]);
        DB::table('chats')->insert([
            'name' => 'Сочи',
            'city' => 'Сочи',
            'date' => $f->dateTimeBetween('now', '+30 days'),
            'type' => 'place',
            'created_at' => \Carbon\Carbon::now()->__toString(),
            'updated_at' => \Carbon\Carbon::now()->__toString(),
        ]);
        DB::table('chats')->insert([
            'name' => '7089',
            'city' => 'Москва',
            'date' => $f->dateTimeBetween('now', '+30 days'),
            'type' => 'flight',
            'created_at' => \Carbon\Carbon::now()->__toString(),
            'updated_at' => \Carbon\Carbon::now()->__toString(),
        ]);
        DB::table('chats')->insert([
            'name' => 'Байкал',
            'city' => 'Иркутск',
            'date' => $f->dateTimeBetween('now', '+30 days'),
            'type' => 'place',
            'created_at' => \Carbon\Carbon::now()->__toString(),
            'updated_at' => \Carbon\Carbon::now()->__toString(),
        ]);
        DB::table('chats')->insert([
            'name' => '104В',
            'city' => 'Краснодар',
            'date' => $f->dateTimeBetween('now', '+30 days'),
            'type' => 'train',
            'created_at' => \Carbon\Carbon::now()->__toString(),
            'updated_at' => \Carbon\Carbon::now()->__toString(),
        ]);

        DB::table('users')->insert([
            'vk_id' => self::VK_OLEG,
            'name' => $f->firstName,
            'surname' => $f->lastName,
            'created_at' => \Carbon\Carbon::now()->__toString(),
            'updated_at' => \Carbon\Carbon::now()->__toString(),
        ]);
        DB::table('users')->insert([
            'vk_id' => self::VK_ARTEM,
            'name' => $f->firstName,
            'surname' => $f->lastName,
            'created_at' => \Carbon\Carbon::now()->__toString(),
            'updated_at' => \Carbon\Carbon::now()->__toString(),
        ]);
        DB::table('users')->insert([
            'vk_id' => self::VK_ALEX,
            'name' => $f->firstName,
            'surname' => $f->lastName,
            'created_at' => \Carbon\Carbon::now()->__toString(),
            'updated_at' => \Carbon\Carbon::now()->__toString(),
        ]);
        DB::table('users')->insert([
            'vk_id' => self::VK_MISHA,
            'name' => $f->firstName,
            'surname' => $f->lastName,
            'created_at' => \Carbon\Carbon::now()->__toString(),
            'updated_at' => \Carbon\Carbon::now()->__toString(),
        ]);
        DB::table('users')->insert([
            'vk_id' => self::VK_DEN,
            'name' => $f->firstName,
            'surname' => $f->lastName,
            'created_at' => \Carbon\Carbon::now()->__toString(),
            'updated_at' => \Carbon\Carbon::now()->__toString(),
        ]);
        DB::table('users')->insert([
            'vk_id' => self::VK_PASHKA,
            'name' => $f->firstName,
            'surname' => $f->lastName,
            'created_at' => \Carbon\Carbon::now()->__toString(),
            'updated_at' => \Carbon\Carbon::now()->__toString(),
        ]);

        $chatController = new ChatController();
        ($chatController)->addVkUserToChat(self::VK_OLEG, 1);
        ($chatController)->addVkUserToChat(self::VK_ARTEM, 1);
        ($chatController)->addVkUserToChat(self::VK_ALEX, 1);
        ($chatController)->addVkUserToChat(self::VK_MISHA, 1);

        ($chatController)->addVkUserToChat(self::VK_PASHKA, 2);
        ($chatController)->addVkUserToChat(self::VK_DEN, 2);
        ($chatController)->addVkUserToChat(self::VK_MISHA, 2);

        ($chatController)->addVkUserToChat(self::VK_DEN, 3);
        ($chatController)->addVkUserToChat(self::VK_ARTEM, 3);

        ($chatController)->addVkUserToChat(self::VK_ALEX, 4);
        ($chatController)->addVkUserToChat(self::VK_MISHA, 4);
        ($chatController)->addVkUserToChat(self::VK_DEN, 4);
        ($chatController)->addVkUserToChat(self::VK_PASHKA, 4);

        ($chatController)->addVkUserToChat(self::VK_ALEX, 5);
        ($chatController)->addVkUserToChat(self::VK_MISHA, 5);
        ($chatController)->addVkUserToChat(self::VK_DEN, 5);
        ($chatController)->addVkUserToChat(self::VK_PASHKA, 5);
        ($chatController)->addVkUserToChat(self::VK_OLEG, 5);

        ($chatController)->addVkUserToChat(self::VK_ALEX, 6);

        $messageController = new MessageController();
        $messageController->addMessage($f->sentence(), self::VK_OLEG, 1);
        $messageController->addMessage($f->sentence(), self::VK_ARTEM, 1);
        $messageController->addMessage($f->sentence(), self::VK_ARTEM, 1);

        $messageController->addMessage($f->sentence(), self::VK_PASHKA, 2);
        $messageController->addMessage($f->sentence(), self::VK_PASHKA, 2);
        $messageController->addMessage($f->sentence(), self::VK_PASHKA, 2);
        $chatController->readChat(self::VK_DEN, 2);
        $messageController->addMessage($f->sentence(), self::VK_MISHA, 2);
        $messageController->addMessage($f->sentence(), self::VK_MISHA, 2);

        $messageController->addMessage($f->sentence(), self::VK_DEN, 3);
        $messageController->addMessage($f->sentence(), self::VK_DEN, 3);
        $messageController->addMessage($f->sentence(), self::VK_DEN, 3);
        $chatController->readChat(self::VK_ARTEM, 3);

        $messageController->addMessage($f->sentence(), self::VK_ALEX, 4);
        $messageController->addMessage($f->sentence(), self::VK_MISHA, 4);
        $messageController->addMessage($f->sentence(), self::VK_DEN, 4);
        $messageController->addMessage($f->sentence(), self::VK_PASHKA, 4);
        $chatController->readChat(self::VK_ALEX, 4);
        $messageController->addMessage($f->sentence(), self::VK_ALEX, 4);
        $messageController->addMessage($f->sentence(), self::VK_ALEX, 4);

        $messageController->addMessage($f->sentence(), self::VK_OLEG, 5);
        $messageController->addMessage($f->sentence(), self::VK_OLEG, 5);
        $messageController->addMessage($f->sentence(), self::VK_OLEG, 5);
        $chatController->readChat(self::VK_PASHKA, 5);

        $messageController->addMessage($f->sentence(), self::VK_ALEX, 6);
    }
}
