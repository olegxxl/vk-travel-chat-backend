<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $guarded = [];

    public function chats()
    {
        return $this->belongsToMany('App\Chat');
    }
}
