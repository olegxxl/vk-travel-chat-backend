<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $guarded = [];

    public function chat()
    {
        return $this->belongsTo('App\Chat');
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::createFromFormat(\Carbon\Carbon::DEFAULT_TO_STRING_FORMAT ,$value)->timestamp;
    }
}
