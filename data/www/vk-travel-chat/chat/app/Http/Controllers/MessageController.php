<?php

namespace App\Http\Controllers;

use App\Events\NewMessage;
use App\User;
use Illuminate\Http\Request;
use App\Message;
use App\Chat;
use App\UnreadMessage;

class MessageController extends Controller
{
    public function addMessage($message, $vkUserId, $chatId)
    {
        $userId = User::where('vk_id', $vkUserId)->first()->id;
        $dbMessage = new Message();
        $dbMessage->message = $message;
        $dbMessage->user_id = $userId;
        $dbMessage->chat_id = $chatId;
        $dbMessage->save();        
        $this->updateUnreadChats($chatId, $dbMessage->id, $userId);
        broadcast(new NewMessage($dbMessage))->toOthers();
    }

    public function getMessagesByChatId($chatId)
    {
        $dbMessages = Chat::find($chatId)->messages()->orderBy('created_at')->get();
        $messages = [];

        foreach ($dbMessages as $dbMessage) {
            $user = User::find($dbMessage['user_id']);
            $messages[$dbMessage->id] = [
                'user' => [
                    'vk_id' => $user->vk_id,
                    'name' => $user->name,
                    'surname' => $user->surname,
                ],
                'text' => $dbMessage->message,
                'timestamp'  => $dbMessage->created_at
            ];
        }

        return $messages;
    }

    public function getMessagesByChatIdFromMessageId($chatId, $messageId)
    {
        $dbMessages = Chat::find($chatId)->messages()->where('id', '>=', $messageId)->orderBy('created_at')->get();
        $messages = [];

        foreach ($dbMessages as $dbMessage) {
            $user = User::find($dbMessage['user_id']);
            $messages[$dbMessage->id] = [
                'user' => [
                    'vk_id' => $user->vk_id,
                    'name' => $user->name,
                    'surname' => $user->surname,
                ],
                'text' => $dbMessage->message,
                'timestamp'  => $dbMessage->created_at
            ];
        }

        return $messages;
    }

    private function updateUnreadChats($chatId, $messageId, $currentUserId)
    {
        $dbUsers = Chat::find($chatId)->users()->where('users.id', '!=', $currentUserId);
        foreach ($dbUsers->get() as $dbUser) {
            $dbUnreadMessage = UnreadMessage::where('chat_id', $chatId)->where('user_id', $dbUser->id)->first();
            if ($dbUnreadMessage) {
                $dbUnreadMessage->unread_messages_count +=1;
            } else {
                $dbUnreadMessage = new UnreadMessage();
                $dbUnreadMessage->unread_messages_count = 1;
                $dbUnreadMessage->chat_id = $chatId;
                $dbUnreadMessage->user_id = $dbUser->id;
                $dbUnreadMessage->first_unread_message_id = $messageId;
            }
            $dbUnreadMessage->save();
        }
    }
}
