<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Chat;

class UserController extends Controller
{
    public function addUser($vkId, $name, $surname)
    {
        $dbUser = new User();
        $dbUser->vk_id = $vkId;
        $dbUser->name = $name;
        $dbUser->surname = $surname;
        $dbUser->save();
        return $dbUser;
    }


    public function getUserByVkId($vkId)
    {
        $dbUser = User::where('vk_id', $vkId)->first();

        if ($dbUser) {
            return $dbUser;
        } else {
            return false;
        }
    }

    public function getUsersByChatId($chatId)
    {
        $dbUsers = Chat::find($chatId)->users()->get();
        $users = [];

        foreach ($dbUsers as $dbUser) {
            $users[$dbUser->id] = [
                'vk_id' => $dbUser->vk_id,
                'name' => $dbUser->name,
                'surname' => $dbUser->surname,
            ];
        }

        return $users;
    }
}
