<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Chat;
use App\User;
use App\UnreadMessage;

class ChatController extends Controller
{
    public function addChat($type, $name, $date, $city)
    {
        $dbChat = new Chat();
        $dbChat->type = $type;
        $dbChat->name = $name;
        $dbChat->date = $date;
        $dbChat->city = $city;
        $dbChat->save();
        return $dbChat;
    }

    public function addVkUserToChat($userVkId, $chatId)
    {
        $dbChat = Chat::where('id', $chatId)->first();
        $user = User::where('vk_id', $userVkId)->first();

        if ($dbChat) {
            $dbChat->users()->attach([$user->id]);
        }
    }

    private function getChatGroupTypeByChatType($chatType)
    {
        switch ($chatType) {
            case 'train':
            case 'flight':
                return 'transport';
            case 'hotel':
                return 'hotel';
            case 'place':
                return 'place';
            default:
                return $chatType;
        }
    }

    public function getChatById($id)
    {
        $dbChat = Chat::find($id);

        if ($dbChat) {
            return [
                'group' => $this->getChatGroupTypeByChatType($dbChat->type),
                'type' => $dbChat->type,
                'title' => $dbChat->name,
                'date' => $dbChat->date,
                'city' => $dbChat->city,
            ];
        } else {
            return false;
        }
    }

    public function getChatsByVkUser($vkId)
    {
        $user = User::where('vk_id', $vkId)->first();
        $dbChats = Chat::all();

        return $this->decorateChatData($dbChats, $user->id, $vkId);
    }

    private function decorateChatData($chats, $userId, $userVkId)
    {
        $decoratedChats = [];

        foreach ($chats as $chat) {
            $users = $chat->users()->get();
            $decoratedChats[$chat->id] = [
                'group' => $this->getChatGroupTypeByChatType($chat->type),
                'type' => $chat->type,
                'title' => $chat->name,
                'date' => $chat->date,
                'city' => $chat->city,
                'users' => $users->map(function ($item) {
                    return $item->vk_id;
                }),
                'isJoined' => $users->contains('id', $userId),
                'newMessages' => $this->getUnreadMessagesCount($userVkId, $chat->id)
            ];
        }

        return $decoratedChats;
    }

    public function readChat($vkUserId, $chatId)
    {
        $unreadMessage = UnreadMessage::where('user_id', User::where('vk_id', $vkUserId)->first()->id)->where('chat_id', $chatId)->first();
        if ($unreadMessage) {
            $unreadMessage->delete();
        }
    }

    public function getUnreadMessagesCount($vkUserId, $chatId)
    {
        $dbUnreadMessage = UnreadMessage::where('chat_id', $chatId)->where('user_id', User::where('vk_id', $vkUserId)->first()->id)->first();
        if (!$dbUnreadMessage) {
            return null;
        }
        return $dbUnreadMessage->unread_messages_count;
    }

    public function getFirstUnreadMessageId($vkUserId, $chatId)
    {
        $dbUnreadMessage = UnreadMessage::where('chat_id', $chatId)->where('user_id', User::where('vk_id', $vkUserId)->first()->id)->first();
        if (!$dbUnreadMessage) {
            return null;
        }
        return $dbUnreadMessage->first_unread_message_id;
    }


}
