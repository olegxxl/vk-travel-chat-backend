<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

class ChatAppController extends Controller
{
    public function getChatList(Request $request)
    {
        $userVkId = $request->get('vk_id');

        $chatList = (new ChatController())->getChatsByVkUser($userVkId);

        return response()->json($chatList);
    }

    public function joinToChat(Request $request)
    {
        $userVkId = $request->get('vk_id');
        $chatId = $request->get('chat_id');

        (new ChatController())->addVkUserToChat($userVkId, $chatId);

        return response()->json(['ok']);
    }

    public function initializeChatForUser(Request $request)
    {
        $chatId = $request->get('chat_id');
        $userVkId = $request->get('vk_id');

        (new ChatController())->readChat($userVkId, $chatId);
        $users = (new UserController())->getUsersByChatId($chatId);
        $messages = (new MessageController())->getMessagesByChatId($chatId);
        $chatInfo = (new ChatController())->getChatById($chatId);

        return response()->json([
            'chat_info'=> $chatInfo,
            'users' => $users,
            'messages' => $messages,
        ]);
    }

    public function addMessage(Request $request)
    {
        $message = $request->post('message');
        $userVkId = $request->post('vk_id');
        $chatId = $request->post('chat_id');

        (new MessageController())->addMessage($message, $userVkId, $chatId);

        return response()->json(['ok']);
    }

    public function getUnreadMessage(Request $request)
    {
        $userVkId = $request->post('vk_id');
        $chatId = $request->post('chat_id');

        $firstUnreadMessageId = (new ChatController())->getFirstUnreadMessageId($userVkId, $chatId);
        if (!$firstUnreadMessageId) {
            return response()->json([]);
        }
        $unreadMessage = (new MessageController())->getMessagesByChatIdFromMessageId($chatId, $firstUnreadMessageId);
        (new ChatController())->readChat($userVkId, $chatId);

        return response()->json($unreadMessage);
    }
}
