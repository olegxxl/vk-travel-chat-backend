<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnreadMessage extends Model
{
    protected $guarded = [];
}
